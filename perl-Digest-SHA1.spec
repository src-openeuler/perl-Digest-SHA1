Name:           perl-Digest-SHA1
Version:        2.13
Release:        28
Summary:        Perl interface to the SHA-1 algorithm
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Digest-SHA1
Source0:        https://cpan.metacpan.org/authors/id/G/GA/GAAS/Digest-SHA1-%{version}.tar.gz
BuildRequires:  gcc findutils glibc-common make perl-interpreter perl-devel perl-generators perl(Test) perl(strict)
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(Digest::base) perl(DynaLoader) perl(Exporter) perl(vars)

%description
The Digest::SHA1 module allows you to use the NIST SHA-1 message digest algorithm from within
Perl programs. The algorithm takes as input a message of arbitrary length and produces as output
a 160-bit "fingerprint" or "message digest" of the input.

%package        help
Summary:        Help document for the perl-Digest-SHA1 package
Buildarch:      noarch

%description    help
Help document for the perl-Digest-SHA1 package.

%prep
%autosetup -n Digest-SHA1-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
chmod -R u+w $RPM_BUILD_ROOT/*

%check
%make_build test

%files
%doc Changes README fip180-1.{gif,html}
%{perl_vendorarch}/Digest/
%{perl_vendorarch}/auto/Digest/

%files help
%{_mandir}/man3/*.3*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 2.13-28
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jun 02 2021 zhaoyao<zhaoyao32@huawei.com> - 2.13-27 
- fixs faileds: /bin/sh: gcc: command not found.

* Wed Feb 19 2020 Ling Yang <lingyang2@huawei.com> - 2.13-26
- Package init
